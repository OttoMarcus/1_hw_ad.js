class Employee {
    constructor(name, age , salary) {{
        this.name = name;
        this.age = age;
        this.salary = salary;
    }}

    get takeName() {
        return this.name
    }
    set takeName(newName) {
        if(typeof newName !== 'number') {
             this.name = newName;
        }
        else {
            console.log("try with characters")
        }
    }


    get takeAge() {
        return this.age
    }
    set takeAge(newAge) {
        if(typeof newAge === "number" && newAge > 18 && newAge !== undefined) {
            this.age = newAge;
        }
        else {
            console.log("request age older 18 (input with numbers)");
        }
    }


    get takeSalary() {
        return this.salary
    }
    set takeSalary(newSalary) {
        this.salary = newSalary;
    }
}

