class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.langList = lang;
    }


    get takeSalary() {
        return super.takeSalary * 3;
    }
    set takeSalary(value) {
        super.takeSalary = value;
    }
}